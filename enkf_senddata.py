# -*- coding: utf-8 -*-
##    # Create a CLIENT TCP/IP socket
##    sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
##    server_address = ('localhost', 10000)
##    sock.connect(server_address)
"""
Created on Mon Nov 17 21:27:31 2014
@author: emilio
"""
import sys, os, numpy as np, platform
funcPath = os.path.dirname(__file__)
sys.path.append(os.path.abspath(os.path.join(funcPath,'..','myfunctions_git')))
import mysocket.mysocket as sock # esyta esta pendiente
import dir_manip as dm
import server_client as sercl


if __name__  == "__main__":
    #%% ----Server Info----- #
    port = 4001
    server = ('134.2.48.89', port+1)    # Windows PC Office: '134.2.48.89' , Ubuntu Laptop: '134.2.185.162', 1024; '10.0.0.2'
                                        # Ubuntu Office: '134.2.49.121', 'localhost', 1024
    #%% ----directories---- #
    homedir = os.path.join(dm.homedir(),'kalmanfilter_git', '_inversion')
    mypath_dict = dm.gendirtree(homedir)

    #%% Define what to extract from the storage folders and make the list to be sent:
    myfiles = [mypath_dict['fld_kkkfiles'], mypath_dict['fld_modobs'], mypath_dict['fld_heads']]
    comname = ['kkkGaus_','mod', 'finalheads']

    list2send = np.array([])
    for curid, ii in enumerate(myfiles): #range(0,len(myfiles), 1):
        mylist, mydir_dummy = dm.getdirs(ii, mystr = comname[curid], fullpath = True, onlylast = False)
        list2send = np.r_[list2send, mylist]

    #%% Create socket (client) and connect to the server:
    client_ssl = sock.client(server)
    buffersize = 4096
    print('Sending binary files...')

    # Send first file name, and then the data within it:
    for zz in list2send:
        filestr = sercl.fmt_filename2send(zz)
        client_ssl.sendall(filestr.encode('utf16')) # Send the file name
        client_ssl.recv(1024).decode('utf16') # Receive 'File name received'
        mybytes = open(zz,'rb').read()

        # Send the content of the file
        client_ssl.sendall(mybytes)
        #client_ssl.recv(1024).decode('utf16')
        client_ssl.send(',filesent'.encode('utf16'))

    client_ssl.send('Allfilessent'.encode('utf16'))
    client_ssl.close()

    print('All binary files sent.')
