""" Initialize matrices for EnKF (KEG).
States (heads, conc) = mm
Parameters = s
Log parameters = Y
"""
import os
import scipy
import scipy.sparse as ssp
import sys
import time
import numpy as np

funcPath = os.path.dirname(__file__)
sys.path.append(os.path.abspath(os.path.join(funcPath, '..', 'myfunctions_git')))
import hgs_grok as hgr
import dir_manip as dm
import matmult.matmul_opt as mm
import kalman as kf
import grid_manip as gm
import gen_fields as gf


def main():
    # ---------------------#
    # %% Define inversion:
    # %% Type of data to integrate, and type and magnitude of time step updating:
    # ---------------------#
    homedir = os.path.join(dm.homedir(), 'kalmanfilter_git', '_inversion')  # Inversion directory:
    mydate = '26062015_synthetic'  # Test date '07072015' '26062015_synthetic_keg'
    inv_name = 'Results_seqassim_%s' % mydate  # Name of current model project
    mymode = 'tr_'  # 'fl_' or 'tr'
    ini_head = 7.0  # Initial head if running flow
    std_dev = 2.5e-6  # This are kg/m3    std_dev = 6.0e-3 -> e.g. std error measurement of 5 mm
    current_time = 1  # Current time step idx to assimilate. Automatic 0 if initialize==true
    #                                             if not, number = (last time step - 1) recorded in results files
    type_update = 'single'  # Type of time update, 'single'(EnKF), 'cum'(restartEnKF) or 'full'(KEG)
    damp_fc = 1.0  # dampening factor for the Kalman gain
    exist_modoutput = True  # Need to obtain model outputs prior to inversion or not
    initialize = False  # Start new inversion or restart previous one
    restart = True  # True: restart the inversion process, False: new inversion
    paralrun = False  # Run fwd models for ini and/or updating in parallel
    ncpus = 4  # CPUs to use for Ens. generation OR matrix multiplication
    # Ini_ensemble = False                        # Initialize an ensemble
    # genfields = False                           # Generate random fields? used when Ini_ensemble is True
    # shrink = 'shrink'                           # How to modify parameter vector if not all grid is relevant
    # Connect2Server = False                      # True: work via a server, False: work locally
    # server = ('134.2.48.89', 4004)              # Server address
    dY_max = 0.50  # max step control bound
    dY_min = 0.30  # min step control bound
    total_iter = 1  # total number of (outer) iterations

    start = time.clock()
    scipy.random.seed()
    """ If you are not sure on what's going on, don't modify the next section """
    # -----------------------#
    # Initialize the process according to settings selected:
    # -----------------------#
    if paralrun is True:  # If paralrun is True paral_matmult must be False
        paral_matmult = False
    elif paralrun is False:
        paral_matmult = True

    # Define directories:
    mypath_dict = dm.gendirtree(homedir, proc_idx=-9999, mode=mymode)
    store_path = os.path.join(homedir, inv_name)
    if not os.path.exists(store_path):
        os.makedirs(store_path)
    fld_modeldata = os.path.abspath(os.path.join(homedir, 'Model_data'))
    fld_refmodel = fld_modeldata
    # fld_refmodel = os.path.abspath(os.path.join(homedir, 'Reference_model'))

    # %% Read grid data:
    grid_filedir = os.path.join(homedir, '%sToCopy' % mymode, 'meshtecplot.dat')
    nnodes, elements = gm.readmesh(grid_filedir, fulloutput=False)

    # Read-in all field measurements if they exist. Dimensions: (Ntimes x Nobspoints)
    measfile = 'Field%s%s.dat' % (mydate, mymode)
    fieldmeas = np.loadtxt(os.path.join(fld_refmodel, measfile))  # Note: Flat observations using .flatten('F')
    all_steptimes = np.loadtxt(os.path.join(fld_modeldata, '_outputtimes%s.dat' % mymode))
    No_timeobs = fieldmeas.shape[0]

    # Read well Labels:
    ObsWellLabels = np.loadtxt(os.path.join(fld_modeldata, '_obswellscoord.dat'), usecols=(0,), dtype='|S')
    assert len(ObsWellLabels) == fieldmeas.shape[1], "# of obs. points in data file =! # obs. wells in well labels file"

    # Initialize the time update loop:
    # -----------------------------------#
    if (initialize is True) and (type_update is not 'full'):
        current_time = 0
    elif (restart is True) and (type_update is not 'full'):
        current_time += 1

    while current_time < No_timeobs:
        if type_update is 'full':
            fieldmeas_length = fieldmeas.shape[0] * fieldmeas.shape[1]
            print('Preparing for KEG-like type of data assimilation step')
        elif type_update is 'cum':
            fieldmeas_length = fieldmeas.shape[1] * (current_time + 1.)
            print('Preparing for Restart EnKF-like type of data assimilation step')
        elif type_update is 'single':
            fieldmeas_length = fieldmeas.shape[1]
            print('Preparing for a standard EnKF data assimilation step')
        print('Initializing time step no. %s (python idx), out of %s...' % (current_time, No_timeobs))

        # Read Parameter fields: Y = log(k), and model outputs (or states e.g. heads or concentration)
        # If is the first iteration, read from received binary files:
        if initialize is True:
            if mymode == 'fl_':  # Read initial fields from binaries
                nel, nreal, Y = hgr.rdparam_bin(mypath_dict['fld_kkkfiles'])

                if exist_modoutput is True:
                    num_modmeas, modmeas = hgr.rdmodelout_bin(mypath_dict['fld_modobs'], mymode=mymode)

                elif exist_modoutput is False:
                    biggrid_elem_ids = np.arange(1, elements + 1, 1)
                    xlen, ylen, zlen, nxel, nyel, nzel, xlim, ylim, \
                        zlim, Y_model, Y_var, beta_Y, lambda_x, lambda_y, \
                        lambda_z = gf.readParam(os.path.join(fld_modeldata, '_RandFieldsParameters.dat'))
                    smallgrid_nodes, smallgrid_elem = gm.sel_grid(grid_filedir, xlim, ylim, zlim)

            elif mymode == 'tr_':
                mylist, mydir = dm.getdirs(store_path, mystr='Y_fl_iter', fullpath=False, onlylast=True)
                thefinaltime = int((mylist.split('step')[-1].split('.')[0]))
                Y = kf.read_iter_data(store_path, myType='Y', my_timest='timestep%.3d' % thefinaltime)
                if len(Y) > elements:
                    Y = Y[1:]
                nel, nreal = Y.shape

                # Read first model outputs if they were already computed:
                try:
                    modmeas = kf.read_iter_data(store_path, myType='ModMeas_%s' % mymode, my_timest='timestep001')
                    # modmeas = np.load(os.path.join(store_path, 'ModMeas_tr_iter001_timestep001.npy'))
                except (UnboundLocalError,
                        FileNotFoundError):  # Make a first model run to get model outputs for transport:
                    print('Model outputs for 1st < dt > in transport not found, running model...')
                    exist_modoutput = False
                    biggrid_elem_ids = ''
                    smallgrid_elem = ''
                    smallgrid_nodes = ''
                    pass

            if exist_modoutput is False:
                modmeas = hgr.caller_getstates(Y, nnodes, elements, homedir, mymode, fieldmeas_length,
                                               current_time, all_steptimes, type_update,
                                               biggrid_elem_ids=biggrid_elem_ids,
                                               smallgrid_elem_ids=smallgrid_elem,
                                               smallgrid_nodes='', headsFile=True, parallel=paralrun, cpus=ncpus,
                                               genfield=False, plot=False, storebinary=False, initial_head=ini_head)
                np.save(os.path.join(store_path, 'ModMeas_%siter%.3d_timestep%.3d' % (mymode, 1, current_time + 1)),
                        modmeas)

            num_modmeas = modmeas.shape[0]
            initialize = False

        # -----------#
        if restart is True:  # Means the inversion was stopped and now we want to read results from last full iteration!

            helpstr = 'timestep%.3d' % current_time
            Y = kf.read_iter_data(store_path, myType='Y_%s' % mymode, my_timest=helpstr)
            nel, nreal = Y.shape
            modmeas = kf.read_iter_data(store_path, myType='ModMeas_%s' % mymode, my_timest=helpstr)
            num_modmeas = modmeas.shape[0]
            restart = False

        elif initialize is False:  # Means process comes from previous iter-step, no need to read files!
            if current_time >= No_timeobs:
                sys.exit('Last time step update performed. Exiting...')

        # %% Read the corresponding set of field observations:
        # selfieldmeas = fieldmeas[current_time, :].flatten('F')  # If initialize is True, take 1st time step measur.
        if type_update == 'single':
            selfieldmeas = fieldmeas[current_time, :].flatten('F')
        elif type_update == 'cumulative':
            selfieldmeas = fieldmeas[0:current_time + 1, :].flatten('F')

        # %% Check consistency in the dim of field measurements and modeled outputs:
        assert len(ObsWellLabels) == len(selfieldmeas), \
            'Dimension mismatch between number of observations (%s) and field measurements (%s)' % (
                len(ObsWellLabels), len(selfieldmeas))

        # %% Apply Gaussian anamorphosis to the modeled and field data:
        # ...

        # %% Some control variables:
        cur_iter = 0  # iteration counter basket

        # %% Initialize parameter-update of the whole ensemble:
        while cur_iter < total_iter:  # and (acceptedReal < nreal):

            print('Inner iteration No. %s started...' % (cur_iter + 1))

            # %% For restart options save a copy of old parameters:
            Y_old = np.copy(Y)

            # %% Errors:
            # R: Cov matrix of measurement error (diag=uncorrelated)(artificial error), (num_modmeas x num_modmeas)
            # E: Measurement noise from a multigaussian dist with mean=0 and std_dev=R, (num_modmeas x nreal)
            # ----------------------------------------------------------------------
            # if mymode == 'fl_':
            #     std_dev = 6.0e-3  # As an example I am assuming a std error measurement of 5 mm on each measurement
            # elif mymode == 'tr_':
            #     std_dev = 2.5e-6        # This are kg/m3

            meanNoise = np.zeros((num_modmeas,))  # Mean value of white noise equal to zero (num_modmeas x 1)
            # # R = np.multiply(np.eye(nmeas,nmeas),std_dev**2) # (nmeas x nmeas)
            # # R = np.diag( (np.multiply(np.ones(nmeas,), std_dev**2)) )
            R = ssp.dia_matrix((np.multiply(np.eye(num_modmeas, num_modmeas), std_dev ** 2)))  # sparse Matrix
            E = np.empty((num_modmeas, nreal))
            for ii in range(0, nreal, 1):
                # E[:,ii] = np.expand_dims(np.random.multivariate_normal(mean,R.toarray()),axis = 1)
                E[:, ii] = np.random.multivariate_normal(meanNoise, R.toarray())

            # Estimate perturbed measurements
            modmeas_pert = modmeas + E

            # %% State auto-covariance matrix Qmm (state means modeled outputs, e.g. heads or concentrations):
            #   Qmm = E[(mu - E[mu]) x (mu - E[mu])' ] (num_modmeas x num_modmeas)
            # ------------------------------------------------------------------------------------------------
            temp = modmeas - np.average(modmeas, axis=1, weights=None, returned=False)[:, np.newaxis]

            Qmm_old = (1. / (nreal - 1)) * (mm.matmul_locopt(temp, temp.T, ncpu=ncpus))
            # # Qmm_old = (1./(nreal-1))*(temp.dot(temp.T))

            # %% New covariance matrix of measurements(state means modeled outputs, e.g. heads or concentrations):
            # Qmm_new = Qmm_old + R
            # ------------------------------------------------------------------------------------------------
            Qmm_new = np.asarray((Qmm_old + R))

            # %% Cross covariance matrix Qym (between parameters and states):
            # Qym = E[(yu - E[yu]) x (mu - E[mu])' ] (nel x num_modmeas)
            # ------------------------------------------------------------------------------------------------
            temp1 = Y - np.average(Y, axis=1, weights=None, returned=False)[:, np.newaxis]
            Qym = (1. / (nreal - 1)) * (mm.matmul_locopt(temp1, temp.T, ncpu=ncpus))  # (nel x num_modmeas)

            # %% Compute the Kalman gain:
            # K = Qym inv( Qmm + R ) (nel x nmeas)
            # ------------------------------------------------------------------------------------------------
            Kgain = damp_fc * (Qym.dot(np.linalg.inv(Qmm_new)))
            # Kgain = np.linalg.lstsq(Qym.T, Qmm_new)[0][:][:]
            # %% Update realizations and get model outputs for new time step t + 1:
            # ------------------------------------------------------------------------------------------------
            if mymode == 'fl_':
                biggrid_elem_ids = np.arange(1, elements + 1, 1)
                xlen, ylen, zlen, nxel, nyel, nzel, xlim, ylim, zlim, Y_model, Y_var, beta_Y, lambda_x, lambda_y, \
                lambda_z = gf.readParam(os.path.join(homedir, 'Model_data', '_RandFieldsParameters.dat'))
                smallgrid_nodes, smallgrid_elem_ids = gm.sel_grid(grid_filedir, xlim, ylim, zlim)
            if mymode == 'tr_':
                biggrid_elem_ids = ''
                smallgrid_elem_ids = ''
                smallgrid_nodes = ''

            Y, modmeas_upd = kf.caller_kfupd_parall(homedir, mymode, type_update, num_modmeas, nnodes,
                                                    elements, dY_max, dY_min, modmeas, Y, current_time, all_steptimes,
                                                    biggrid_elem_ids, smallgrid_elem_ids, smallgrid_nodes,
                                                    selfieldmeas, R, Kgain, Y_old, E,
                                                    headsFile='', parallel=paralrun, cpus=ncpus,
                                                    parmatmult=paral_matmult, initial_head=ini_head)
            # %% Calculate conditional covariance matrix of parameter, or error covariance ( nel x nel )
            # Qyy = E[(Y_c - E[Y_c])(Y_c - E[Y_c])']
            # -------------------------------------------------------------------------------------------
            # temp2 = (Y.transpose() - np.average(Y, axis=1, weights=None, returned=False)).transpose()
            # Qyy_c = (1. / (nreal - 1)) * (mm.matmul_locopt(temp2, temp2.T, ncpu=ncpus))

            # %% Store stuff:
            np.save(os.path.join(store_path, 'Y_%siter%.3d_timestep%.3d' % (mymode, cur_iter + 1, current_time + 1)),
                    Y)  # Updated log parameter field
            np.save(os.path.join(store_path, 'Qym_%siter%.3d_timestep%.3d' % (mymode, cur_iter + 1, current_time + 1)),
                    Qym)  # Cross covariance matrix Qsy
            # np.save(
            #    os.path.join(store_path, 'Qyy_c_%siter%.3d_timestep%.3d' % (mymode, cur_iter + 1, current_time + 1)),
            #    Qyy_c)  # Error covariance of parameters
            np.save(
                os.path.join(store_path, 'ModMeas_%siter%.3d_timestep%.3d' % (mymode, cur_iter + 1, current_time + 1)),
                modmeas_upd)  # Updated modeled states (i.e. modeled measurements)

            cur_iter += 1
            print(u'Inner iteration No. {0:d} completed.'.format(cur_iter))
            print('It took %f seconds to go through full inner iteration' % (time.clock() - start))

        print('Time step No. %s (out of %s) finalized... (python indexing)...' % (current_time, No_timeobs))
        current_time += 1

        # Make new predictions at time t + 1:
        print('Making new model predictions...')
        if current_time < No_timeobs:
            if (mymode == 'fl_') and ((current_time + 1) == No_timeobs):
                headsfile = True
                storebin = True
            else:
                headsfile = False
                storebin = False
            modmeas = hgr.caller_getstates(Y, nnodes, elements, homedir, mymode, fieldmeas.shape[1], current_time,
                                           all_steptimes,
                                           type_update, biggrid_elem_ids=biggrid_elem_ids,
                                           smallgrid_elem_ids=smallgrid_elem_ids, smallgrid_nodes=smallgrid_nodes,
                                           headsFile=headsfile, parallel=paralrun, cpus=ncpus, genfield=False,
                                           plot=False, storebinary=storebin, initial_head=ini_head)

            # Save new data sets with updated timestep in case of restarting:
            np.save(os.path.join(store_path, 'ModMeas_%siter%.3d_timestep%.3d' % (mymode, cur_iter, current_time + 1)),
                    modmeas)

        elif current_time >= No_timeobs:
            print(' Work done, preparing to exit...')
            sys.exit('Terminated')


if __name__ == '__main__':
    main()
