""" Initialize matrices for EnKF (KEG).
    States (heads, conc) = mm
    Parameters = s
    Log parameters = Y """
import sys,os, numpy as np, matplotlib.pyplot as plt, time, scipy, scipy.sparse as ssp, scipy.stats as sst, multiprocessing as mp, pdb
funcPath = os.path.dirname(__file__)
from itertools import repeat
sys.path.append(os.path.abspath(os.path.join(funcPath,'..','myfunctions_git')))
import hgs_grok as hgr
import dir_manip as dm
import matmult.matmul_opt as mm
import kalman as kf
import grid_manip as gm
import gen_fields as gf
import time

#import server_client as sercl
#import TransfStatsFunctions as TSF

def main():
    #---------------------#
    #%% Define inversion:
    #%% Type of data to integrate, and type and magnitude of time step updating:
    #---------------------#
    homedir = os.path.join(dm.homedir(),'kalmanfilter_git')
    Inv_name = 'Results_seqassim' # Name of the current model project
    mymode = 'fl_'              #'fl_'
    type_update = 'single'      # Typoe of time updating, single time, or 'cumulative'
    Ini_ensemble = False        # Initialize an ensemble
    initialize = True           # Start a new inversion or restart a previous one...
    restart = False              # True: restart the inversion process, False: new inversion
    shrink = 'shrink'           # Manipulate parameters for the grid outside the area of interest
    genfields = False           # Generate random fields? used when Ini_ensemble is True
#    parallel_fwdrun = False      # Run forward model for initialization and/or updating in parallel
    Connect2Server = False      # True: work via a server, False: work locally
    port = 4004
    server = ('134.2.48.89', port)
    ncpus = 2                    # No of CPUs to use for the ensamble generation
    current_time = 0            # Current time step index to assimilate (0 if we initialize).
                                # Give the number of the last time step - 1, recorded in results files
    #-----------------------#
    #-----------------------#
    
    scipy.random.seed()

    """ If you are not sure on what's going on, don't modify the next section """

    #%% Define directories:
    mypath_dict = dm.gendirtree(homedir, proc_idx =-9999, mode=mymode)
    store_path = os.path.join(homedir, Inv_name)
    if not os.path.exists(store_path):
        os.makedirs(store_path)
    fld_refmodel = os.path.abspath(os.path.join(homedir, 'Reference_model'))
    fld_modeldata = os.path.abspath(os.path.join(homedir, 'Model_data'))

    #%% Read in all field measurements, Dimensions: (Ntimes x Nobspoints)
    measfile = 'Field%s.dat' %mymode
    fieldmeas = np.loadtxt(os.path.join(fld_refmodel, measfile)) # Note: Flat observations using .flatten('F')
    all_steptimes = np.loadtxt(os.path.join(fld_modeldata, '_outputtimes%s.dat' %mymode))
    No_timeobs = fieldmeas.shape[0]
    grid_filedir = os.path.join(homedir, '%sToCopy'%mymode, 'meshtecplot.dat' )
    nnodes, elements = gm.readmesh(grid_filedir, fulloutput = False)
    
    
    biggrid_elem_ids = np.arange(1,elements+1,1)
    xlen, ylen, zlen, nxel, nyel, nzel, xlim, ylim, zlim, Y_model, Y_var, beta_Y, lambda_x, lambda_y, lambda_z = gf.readParam(os.path.join(homedir, 'Model_data', '_RandFieldsParameters.dat'))
    smallgrid_nodes, smallgrid_elem_ids = gm.sel_grid(grid_filedir, xlim, ylim, zlim)

    nel, nreal, Y = hgr.rdparam_bin(mypath_dict['fld_kkkfiles'])
    current_time = 0

    Y = Y[:,0:4]
    
    
            
    start = time.clock()
    modmeas1 = caller_getstates(Y, nnodes, elements, homedir, mymode, fieldmeas.shape[1], current_time, all_steptimes, type_update, biggrid_elem_ids = biggrid_elem_ids, smallgrid_elem_ids=smallgrid_elem_ids, smallgrid_nodes = smallgrid_nodes, headsFile = '', parallel = False, cpus = '')
    tot_time = time.clock()-start
    print (tot_time)
    
    start2 = time.clock()
    modmeas2 = caller_getstates(Y, nnodes, elements, homedir, mymode, fieldmeas.shape[1], current_time, all_steptimes, type_update, biggrid_elem_ids = biggrid_elem_ids, smallgrid_elem_ids=smallgrid_elem_ids, smallgrid_nodes = smallgrid_nodes, headsFile = '', parallel = True, cpus = 4)
    tot_time = time.clock()-start2
    print (tot_time)
    print('a')
    #np.save(os.path.join(store_path,'ModMeas_%siter%.3d_timestep%.3d'%(mymode, 1, current_time+1)),modmeas)

def caller_getstates(Y, nnodes, elements, homedir, mymode, fieldmeas_len, current_time, all_steptimes,
                     type_update, biggrid_elem_ids = '', smallgrid_elem_ids='', smallgrid_nodes = '', headsFile = '', parallel = False, cpus = ''):
    nel, nreal = Y.shape
    modmeas = np.zeros((fieldmeas_len, nreal))
    if parallel is False:
        for ii in range(0,nreal):
            modmeas[:,ii],idx_dummy =  getstates_parall(ii, Y, nnodes,elements,homedir, '', mymode,
                                         current_time,all_steptimes, type_update, biggrid_elem_ids, smallgrid_elem_ids,smallgrid_nodes)
    elif parallel is True:
        mypool = mp.Pool(cpus)
        modmeas_temp =  mypool.starmap(getstates_parall, zip(np.arange(0,nreal,1), repeat(Y), repeat(nnodes), repeat(elements),
                                                  repeat(homedir), repeat(headsFile),repeat(mymode), repeat(current_time), repeat(all_steptimes),
                                                    repeat(type_update), repeat(biggrid_elem_ids), repeat(smallgrid_elem_ids), 
                                                    repeat(smallgrid_nodes)))
        for ii in range(0,len(modmeas_temp)):
            modmeas[:,modmeas_temp[ii][-1]] = np.asarray(modmeas_temp[ii][0])
    
    return modmeas

def getstates_parall(ii, Y, nnodes, elements, homedir, headsFile, mymode, current_time, all_steptimes, type_update, biggrid_elem_ids, smallgrid_elem_ids,smallgrid_nodes):
    """ Call the getstates function but in a parallel fashion"""
    
    mypath_dict = dm.gendirtree(homedir, proc_idx = ii+1, mode = mymode)
 
    if os.listdir(mypath_dict['fld_mode']) == []:
        #hgr.prepfwd(ii, homedir, mymode, headsFile = 'finalheadsFlow_%.5d.dat'%(ii+1))
        hgr.prepfwd(ii, homedir, mymode, headsFile = headsFile)

    hgr.addt2grok(ii+1, homedir, mypath_dict, current_time, all_steptimes, mode = mymode, mytype = type_update)
    
    modmeas = hgr.getstates(homedir, ii, mymode, nnodes, elements, all_steptimes, biggrid_elem_ids, smallgrid_elem_ids, # here I have an error
                            inner_gr_nodes = smallgrid_nodes, Y_i_upd = np.exp(Y[:,ii]), curtimestep = current_time,
                            genfield = False, plot = False, storebinary = False, mytype = type_update)
    return modmeas, ii



# Work for Kenya: Incorporate and test this functions into the main algorithm
def caller_kfupd_parall():

    nel, nreal = Y.shape
    modmeas_upd = np.zeros((modmeas.shape))
    Y_new = np.zeros((Y.shape))

    if parallel is False:
        for zz in range(0,nreal):
            Y_new[:,zz], modmeas_upd[:,zz], zz_idx = kfupd_parall()
            assert zz_idx == zz_idx, 'Oh oh, it seems there is a mess in the tracking system of realizations!!!'

    elif parallel is True:
        mypool = mp.Pool(cpus)


def kfupd_parall():

    mypath_dict = dm.gendirtree(homedir, proc_idx = tt+1, mode = mymode)
 
    if os.listdir(mypath_dict['fld_mode']) == []:
        #hgr.prepfwd(ii, homedir, mymode, headsFile = 'finalheadsFlow_%.5d.dat'%(ii+1))
        hgr.prepfwd(tt, homedir, mymode, headsFile = headsFile)

    hgr.addt2grok(tt+1, homedir, mypath_dict, current_time, all_steptimes, mode = mymode, mytype = type_update)

    Y_tt, modmeas_upd_tt = kf.update_step(tt, homedir, nreal, num_modmeas, nnodes, elements, all_steptimes, biggrid_elem_ids,
                                            smallgrid_elem_ids, smallgrid_nodes, selfieldmeas, modmeas[:,tt], R, Kgain, Y,
                                            Y_old[:,tt], E[:,tt], dY_max, dY_min, mymode = mymode, curtimestep = current_time,
                                            mytype = type_update)

    return Y_tt.squeeze(), modmeas_upd_tt, tt




if __name__ == '__main__':
    main()
    
