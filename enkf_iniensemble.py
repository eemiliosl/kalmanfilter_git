""" Run X number of realizations to get datasets for the EnKF. It is
written in such a way that a socket (client) is created and when the right IP
of a SERVER is provided, it will establish communications """
import sys
import os
import numpy as np
import time
import multiprocessing as mp
funcPath = os.path.dirname(__file__)
sys.path.append(os.path.abspath(os.path.join(funcPath, '..', 'myfunctions_git')))
import mysocket.mysocket as sock  # pendent
import par_real as pr
import dir_manip as dm

if __name__ == '__main__':
    # ---------------------#
    # %% Define inversion:
    # ---------------------#
    homedir = os.path.join(dm.homedir(), 'kalmanfilter_git', '_inversion')
    #                               # 'C:\PhD_Emilio\Emilio_PhD\_CodeDev_bitbucket\kalmanfilter_git'
    mode = 'fl_'                    # 'fl_' or 'tr_'
    mytype = 'single'               # Type of time update, 'single'(EnKF), 'cum'(restartEnKF) or 'full'(KEG)
    initial_head = 20.0
    grid_filedir = os.path.join(homedir, '%sToCopy' % mode, 'meshtecplot.dat')
    Ini_ensemble = True
    genfields = True
    parallel_fwdrun = True          # Run forward model for initialization and/or updating in parallel
    Connect2Server = False          # True: work via a server, False: work locally
    nFields = 300                   # Used only if Connect2Server == False
    ncpu = 4                        # No of CPUs to use for the ensemble generation
    port = 4001
    server = ('134.2.48.89', port)  # Windows PC Office: '134.2.48.89'
    #                                 Ubuntu Laptop: '134.2.185.162', 1024; '10.0.0.2'
    #                                 Ubuntu Office: '134.2.49.121'
    #                                 'localhost', 1024
    # ----------------------#
    # ----------------------#
    # %% Initialize client socket connection to receive the indices to work on:
    start = time.clock()
    flag = 'stillworking'

    while flag == 'stillworking':

        if Connect2Server is True:
            indicesDone = False
            processes, flag = sock.senddata(server, ncpu)
            print('working ids %s (flag:%s)' % (processes, flag))

        elif Connect2Server is False:
            print('Initializing the ensemble in a local computer...')
            mypath_dict = dm.gendirtree(homedir, proc_idx=-9999, mode='')
            # processes = random.sample(['aa','bb','cc','xx','ss','tt','dd'],nFields)
            processes = np.arange(1, nFields + 1, 1).astype('str')

        str2pass = [s + '-%s-%s-%s-%s-%s-%s-%s'
                    % (homedir, mode, Ini_ensemble, genfields, grid_filedir, initial_head, mytype) for s in processes]

        # %% Perform realizations using the defined process Indices:
        if flag == 'done':
            print('No more work received')
        else:

            if parallel_fwdrun is False:
                for ii in str2pass:
                    indicesDone = pr.GenReal(ii)
            elif parallel_fwdrun is True:
                pool = mp.Pool(ncpu)
                indicesDone = pool.map(pr.GenReal, str2pass)
                pool.close()
                pool.join()

            print('-- %f seconds to create %d fields (with model outputs) --' % (time.clock() - start, len(processes)))

            if Connect2Server is False:
                flag = 'done'
