# Steps to create a self-signed with openssl in the command line:
#    from: https://devcenter.heroku.com/articles/ssl-certificate-self
#$ which openssl      if path is not returned openssl need to be installed
#$ openssl x509 -req -days 365 -in server.csr -signkey server.key -out server.crt
#
#For a private key and a signing request:
#$ openssl genrsa -des3 -out server.orig.key 2048
#$ openssl rsa -in server.orig.key -out server.key
#$ openssl req -new -key server.key -out server.csr
#$ openssl x509 -req -days 365 -in server.csr -signkey server.key -out server.crt
""" Build up the server that will distribute the work to generate realizations
Author: EESL, 2015
"""
import socket, ssl, os, sys, numpy as np, datetime, shutil
funcPath = os.path.dirname(__file__)
sys.path.append(os.path.abspath(os.path.join(funcPath,'..','myfunctions_git')))
import mysocket.mysocket as sock # esyta esta pendiente
import dir_manip as dm
import server_client as sercl

if __name__  == "__main__":

    #%% Ensemble data
    #---------------#
    homedir = os.path.join(dm.homedir(),'kalmanfilter_git', '_inversion')
    mode_flag = 'genreal'                # recvdata: Receive data mode   genreal: distribute work mode
    nFields = 500
    NoComputers = 2                 # Controller to wait for the last realization, before start receiving data

    #%% Server data:
    #---------------#
    port = 4001
    myip = sock.getIP()
    print('Host name (IP): %s ' % myip)

    #%% Directories:
    #---------------#
    mypath_dict = dm.gendirtree(homedir, proc_idx =-9999, mode='')

    if mode_flag == 'genreal':

        #%% Initialize indices:
        idall = np.arange(1,nFields+1,1)
        iddone = np.array([])
        ##basket = 0

        #%% Create TCP/IP SERVER socket and initialize server:
        server = sock.server(myip, port)

        #%% Waiting for connections and distribute work:
        while len(idall) >= len(iddone): ##while basket <= nFields + (NoComputers-1):

            # Wait for a connection:
            print('Waiting for a connection. Date: %s...' %(datetime.datetime.now()))

            # Accept connection from a client:
            connection, client = server.accept()
            # wrap the socket for a secured connection using SSL
            try:
                connstream = ssl.wrap_socket(connection, server_side=True, certfile="cert",keyfile="key")
                print('Succesful SSL connection! ')
            except IOError:
                print('SSL connection was not possible')

            try:
                num_cpu = connstream.recv(1024).decode('utf16')
                print(client,'connected with %s CPUs. Date: %s' %(num_cpu, datetime.datetime.now()))

                #%% Create proper indices to distribute work:
                id2send = sercl.mk_id2send(idall, iddone, num_cpu)
                print('Realization indices to send: ', id2send)

                #%% Send the indices if there are still left to send:
                if len(id2send) > 0:
                    flag_work = 'stillworking'

                    # Prepare INDICES with a proper string format:
                    message = id2send.astype(str)
                    ids_message = sercl.fmt_id2send(id2send, message)

                    # Send indices to the client:
                    connstream.send(ids_message.encode('utf16'))
                    connstream.send(flag_work.encode('utf16'))

                    # Receive indices back from the client and add them to iddone:
                    idrecv = connstream.recv(1024).decode('utf16')
                    iddone = sercl.fmt_idrecv(idrecv, iddone)
                    ##basket = len(iddone)

                elif len(id2send) == 0:
                    print('All realizations ids sent')
                    flag_work = 'done'
                    message = ['work,','completed']
                    message = ''.join(message)
                    iddone = np.r_[iddone, 1]

                    # Send finilizing message so clients can get disconnected:
                    connstream.sendall(message.encode('utf16'))
                    connstream.sendall(flag_work.encode('utf16'))
                    ##basket = basket + 1

            except (KeyboardInterrupt,socket.error, IOError, OSError, EOFError, IndexError, ValueError,WindowsError) as err:
                    repr(err)
                    raise

        try:
            connstream.shutdown(socket.SHUT_RDWR)
            connstream.close()
            server.close()
            print('Shutting down server')
            #server_sock.shutdown(socket.SHUT_RDWR)
            mode_flag = 'recvdata'
        except:
            print('Unexpected error, contact your clients!!!')
            raise


#"""------------------------------------------------------------------------------#
#      Reinitialize server to receive all generated data:
#------------------------------------------------------------------------------"""
    if mode_flag == 'recvdata':
        #%% Create TCP/IP SERVER socket:
        server = sock.server(myip, port+1)
        buffersize = 4096
        basket = 0

        inputs = [server]
        outputs = []

        while basket < nFields:
            # Wait for a connection:
            print('Waiting to receive data... (Time: %s)...' %(datetime.datetime.now()))
            connection, client = server.accept()

            # wrap the socket for a secured connection using SSL
            try:
                connstream = ssl.wrap_socket(connection, server_side=True, certfile="cert",keyfile="key")
                print('Succesful SSL connection! ')
            except IOError:
                print('SSL connection was not possible')

            # Start receiving data
            try:
                print('Receiving data from:', client, ', Date: %s' %(datetime.datetime.now()))
                print('Writing files...')

                while True:
                    myFilename = connstream.recv(1024).decode('utf16') # Recieve filename
                    if myFilename == 'Allfilessent':
                        break
                    connstream.sendall('File name received'.encode('utf16'))

                    # Select the folder and create file:
                    mypath = sercl.storedir(myFilename, mypath_dict)
                    myfile = open(os.path.join(mypath,myFilename),'wb')
                    mystr = ''
                    while True:
                        data = connstream.recv(buffersize) # Receive the file content
                        try:
                            mystr = data.decode('utf16')
                        except:
                            pass
                        if mystr:
                            if mystr.endswith(',filesent'):
                                break
                        myfile.write(data)

                    myfile.flush()
                    myfile.close()

                print('Finished files from client:,',client, ' Date: %s' %(datetime.datetime.now()))
                print('Client disconnected')

                Files = np.r_[  os.listdir(mypath_dict['fld_reckkk'] ),
                                os.listdir(mypath_dict['fld_recmodobs']),
                                os.listdir(mypath_dict['fld_recheads'])]

                basket = len(Files)/3

            except (KeyboardInterrupt,socket.error, IOError, OSError, EOFError, IndexError, ValueError,WindowsError):
                    connstream.shutdown(socket.SHUT_RDWR)
                    connstream.close()
                    sys.exit("You pressed Ctrl+C")

            finally:
                # Always clean up the connection
                print('Closing connection...\n')
                connstream.shutdown(socket.SHUT_RDWR)
                connstream.close()

        connstream.close()
        server.close()
        print('Data from all realizations received (a total of %d files)...' %len(Files))

        print('Moving received files to the corresponding folders...')


        for ii in os.listdir(mypath_dict['fld_recheads']):
            shutil.copyfile(os.path.join(mypath_dict['fld_recheads'], ii), os.path.join(mypath_dict['fld_heads'], ii))

        for ii in os.listdir(mypath_dict['fld_recmodobs']):
            shutil.copyfile(os.path.join(mypath_dict['fld_recmodobs'], ii), os.path.join(mypath_dict['fld_modobs'], ii))

        for ii in os.listdir(mypath_dict['fld_reckkk']):
            shutil.copyfile(os.path.join(mypath_dict['fld_reckkk'], ii), os.path.join(mypath_dict['fld_kkkfiles'], ii))

        # Remove receiving folder?:
        del_recfolder = str(input('Delete datarecieved folder? y/n: '))
        if del_recfolder == 'y':
            shutil.rmtree(mypath_dict['fld_recdata'])
            print('< datarecieved > folder removed')
        else:
            print('< datarecieved > folder NOT removed')