import sys
import os
import scipy.sparse as ssp
import matplotlib.pylab as plt
funcPath = os.path.dirname(__file__)
sys.path.append(os.path.abspath(os.path.join(funcPath, '..', 'myfunctions_git')))
import dir_manip as dm
import grid_manip as gm
import hgs_grok as hg
import numpy as np
import gen_fields as gf


def main():
    # Define variables:

    mainPath = r'C:\PhD_Emilio\Emilio_PhD\_CodeDev_bitbucket\kalmanfilter_git\myModels'  # os.path.dirname(__file__)
    curKFile = 'kkkGaus.dat'
    mymodel = '_tr_26062015_synthetic'  # '_tr_20062015_synthetic_b3top_fluor'
    Flowdir = '_fl_26062015_synthetic'  # Used only if transport is to be run
    modelDataFolder = 'ModelData_26062015_synthetic'
    std_dev = 2.5e-6  # As an example assumed a std error measurement of 1.0 mm on each measurement 3.0e-3, tr = 2.5e-6
    ini_head = ''
    # mymode = 'fl_'  # 'tr_'

    mytimes, mymodOutput = hg.run_refmodel(mainPath, curKFile, Flowdir, modelDataFolder, mymodel, mymode='tr_',
                                           runmodel=False, std_dev=std_dev, ini_head=ini_head, ret_modout=True)

    fig, ax = plt.subplots(1)
    for ii in range(0, mymodOutput.shape[-1]):
        ax.plot(mytimes, mymodOutput[:, ii], '-x')
    ax.set_title('model outputs for: %s' % mymodel)
    ax.set_ylabel('Conc [$g/l$]')
    ax.set_xlabel('Time [s]')
    textstr = '$\sigma=%.2e$\n$Ini head=%s$' % (std_dev, ini_head)
    props = dict(boxstyle='round', facecolor='wheat', alpha=0.5)
    # place a text box in upper left in axes coordinates
    ax.text(0.05, 0.7, textstr, transform=ax.transAxes, fontsize=11,
            verticalalignment='top', bbox=props)
    plt.grid(True)

    print('Done')
if __name__ == '__main__':
    main()
