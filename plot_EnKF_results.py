# -*- coding: utf-8 -*-
"""
Functions to deal with GROK and HGS
Created on Wed Apr 7 11:50:00 2015
@author: eesl
"""
import matplotlib.pyplot as plt
import numpy as np
import itertools
import sys
import os
import pdb

funcPath = os.path.dirname(__file__)
sys.path.append(os.path.abspath(os.path.join(funcPath, '..', 'myfunctions_git')))
import gen_fields as gf
import dir_manip as dm
import grid_manip as gm
import pycleaner.myplot as pcplt
import mystats as mysst

plt.rcParams.update({'figure.max_open_warning': 0})


def main():
    # ----------------------------------------------------------------- #
    # Settings:
    values = 'parameter'  # 'parameter' 'observation' 'cross-covariance'
    mydate = '26062015_synthetic'  # '26062015' 'synth_windows' 'synthetic'
    mymode = 'fl_'  # 'tr_'
    perensemble = False

    # Directories:
    hostdir = os.path.join(dm.homedir(), 'kalmanfilter_git')
    modeldir = os.path.join(hostdir, 'myModels')
    resdir = os.path.join(hostdir, '_inversion', 'Results_seqassim_%s' % mydate)
    figdir = os.path.join(resdir, 'figs')
    grid_filedir = os.path.join(modeldir, '_%s%s' % (mymode, mydate), 'meshtecplot.dat')

    # Plotting settings:
    # marker = itertools.cycle(( '8', '^','s','p','D','h', 'H'))
    # colors = itertools.cycle(('k', 'b', 'g', 'c', 'y',))

    # ---------------------------------------------------------------- #
    # Do not modify the code below if you are not sure whats going on
    # ---------------------------------------------------------------- #
    # Initiate relevant variables:
    lstresdir = []
    if values == 'parameter':
        mystr = 'Y'
    elif values == 'observation':
        mystr = 'ModMeas'
    elif values == 'covariance':
        mystr = 'Qmy'

    for ss in os.listdir(resdir):
        if ss.startswith('%s_%s' % (mystr, mymode)):
            lstresdir.append(ss)
    lstresdir.sort()

    # Load field or synthetic observations:
    if os.path.isdir(figdir) is False:
        os.makedirs(figdir)
    fielddata = np.loadtxt(os.path.join(modeldir, 'ModelData_%s' % mydate, 'Field%s%s.dat' % (mydate, mymode )))
    outputtimes = np.loadtxt(os.path.join(modeldir, 'ModelData_%s' % mydate, '_outputtimes%s.dat' % mymode))
    mymodeldimensions = os.path.join(modeldir, 'ModelData_%s' % mydate, '_RandFieldsParameters.dat')

    # Load model grid geometry:
    xlen, ylen, zlen, nxel, nyel, nzel, xlim,\
          ylim, zlim, Y_model, Y_var, beta_Y,\
          lambda_x, lambda_y, lambda_z = gf.readParam(mymodeldimensions)

    nnodes, elements = gm.readmesh(grid_filedir, fulloutput=False)
    node_coord = np.genfromtxt(grid_filedir, skip_header=3, max_rows=nnodes, usecols=(0, 1, 2))
    if mymode == 'fl_':     # Separate inner from big grid if working with flow
        biggrid_elem_ids = np.arange(1, elements + 1, 1)
        smallgrid_nodes, smallgrid_elem = gm.sel_grid(grid_filedir, xlim, ylim, zlim)

    # Load synthetic parameters if is the case:
    if 'synthetic' in mydate:
        # orig_field = np.loadtxt(os.path.join(modeldir, 'Reference_model', '%s' % mymode, 'kkkGaus.dat'), usecols=(1,))
        orig_field = np.loadtxt(os.path.join(grid_filedir, '..', 'kkkGaus.dat'), usecols=(1,))
        if mymode == 'fl_':
            orig_field = orig_field[smallgrid_elem]
            # node_coord = node_coord[smallgrid_nodes, :]
    #    sel_values, sel_elements_reshaped = pcplt.plot2dsections(grid_filedir, np.array([35, 65.]),
    #                                                              np.array([54., 66.]), np.array([5.7, 6.25]),
    #                                                              orig_field,
    #                                                              dx=.5, dy=.5, typedata='elemental', scalemin=0.0002,
    #                                                              scalemax=0.01,
    #                                                              saveim=True, image_output='interp1.png',
    #                                                              savetxt=True, ascii_output='interp1.txt')

    # Load defined inversion results:
    if values == 'parameter':
        # Plot pdf of ensemble parameters and if is the case, also the synthetic parameters
        for idx, xx in enumerate(lstresdir):
            myres = np.load(os.path.join(resdir, xx))
            if 'fl_' in mymode:
                myres = myres[1:]
            # Look at single fields if True:
            if perensemble is True:
                plt.figure(1)
                for zz in np.arange(0, myres.shape[-1]):
                    plt.hist(myres[:, zz], histtype='step', normed=False, color='k', alpha=0.5, bins=16)
            if 'synthetic' in mydate:
                plt.hist(np.log(orig_field), histtype='step', normed=False, color='r', label='synthetic', bins=16)
            plt.hist(myres.mean(axis=1), histtype='step', color='b', label='Ensemble mean', normed=False, bins=16)
            plt.title('Ensemble %d: t = %s s' % (idx + 1, outputtimes[idx]))
            plt.grid(True)
            plt.legend(loc='best')
            plt.savefig(os.path.join(figdir, 'pdf_%s_Ensemble%d_t_%s_sec.png' % (mydate, idx + 1, outputtimes[idx])), dpi = 400, format = 'png')
            plt.show()

        # If synthetic case, calculate and plot some statistics:
        if 'synthetic' in mydate:
            for idx, xx in enumerate(lstresdir):
                myres = np.load(os.path.join(resdir, xx))
                if mymode is 'fl_':
                    myres = myres[1:]
                nash = mysst.nash_sutcliffe(myres.mean(axis=1), observed=np.log(orig_field))
                pears = mysst.Pearson(myres.mean(axis=1), np.log(orig_field))
                print('Time step %s--> NS-coeff: %s\nPearsons: %s' %(idx, nash, pears))
                plt.plot(np.log(orig_field), myres.mean(axis=1), 'rx')
                plt.title('Synthetic vs modeled parameters')
                plt.xlabel('Original parameters')
                plt.ylabel('Modeled parameters')
                plt.grid(True)
                plt.savefig(os.path.join(figdir, 'calibplot_Ensemble%d_t_%s_sec.png' % (idx + 1, outputtimes[idx])),dpi=400, format='png')
                plt.show()
                plt.close()

                # Relative errors:
                rel_error = mysst.relative_error(myres.mean(axis=1), np.log(orig_field))
                plt.plot(rel_error)
                plt.show()

                # Plot some slices:


            plt.plot(myres.mean(axis=1), label='Mean K field%s' % xx[-7:-4])
            if 'synthetic' in mydate:
                plt.plot(np.log(orig_field), label='Orig K field', alpha=0.6)

            plt.xlabel('Grid element number')
            plt.ylabel('Parameter value')
            plt.legend(loc='best')
            plt.grid(True)
            plt.show()


            for xx in lstresdir:
                myres = np.load(os.path.join(resdir, xx))
                myres = myres[1:]
                plt.plot(np.log(orig_field), myres.mean(axis=1), 'rx')

                plt.xlabel('Original parameters')
                plt.ylabel('Modeled parameters')
                plt.legend(loc='best')
                plt.grid(True)
                plt.show()

                # Get some cross sections:

    # Load modeled observations:
    if values == 'observation':

        rmse_all = np.array([])
        relative_error_all = np.array([])
        for xx in range(0, len(outputtimes), 1):
            ModelHeads = np.load(os.path.join(resdir, lstresdir[xx]))
            meanheads = np.mean(ModelHeads, axis=1)
            stdheads = np.std(ModelHeads, axis=1)
            rmse = np.sqrt(np.mean((meanheads - fielddata[xx, :]) ** 2))
            rmse_all = np.r_[rmse_all, rmse]
            relative_error = mysst.relative_error( meanheads, fielddata[xx, :])
            relative_error_all = np.r_[relative_error_all, np.mean(relative_error)]
            for ii in range(0, fielddata.shape[-1], 1):
                plt.figure(ii + 1, figsize=(10, 5), dpi=100)
                plt.plot(np.ones((ModelHeads.shape[-1], 1)) * outputtimes[xx], ModelHeads[ii, :], 'k.', alpha=.5)
                plt.plot(outputtimes[xx], meanheads[ii], 'gx')
                plt.plot(outputtimes[xx], fielddata[xx, ii], 'rx', label='Obs No:%s' % (ii + 1))

        plt.show()
        plt.plot(rmse_all, 'b-o', label='RMSE of observations')
        plt.show()
        # Left here on tuesday 29.03.2016. I want to plot nicely the mod, vs field observations

    if values == 'covariance':

        for xx in lstresdir:
            myres = np.load(os.path.join(resdir, xx))
            ##    lstresdir = []
            ##    for ss in os.listdir(resdir):
            ##        if ss.startswith('ModMeas_%s'%mymode):
            ##            lstresdir.append(ss)
    for yy in range(0, len(lstresdir)):
        ModelHeads = np.load(os.path.join(resdir, lstresdir[yy]))
        meanheads = np.mean(ModelHeads, axis=1)
        plt.figure(1)
        plt.plot(np.ones((len(meanheads), 1)) * outputtimes[yy], meanheads, linestyle='', marker='o', c='yellow',
                 ms=3.5, lw=0.01)
        rmse = np.sqrt(np.mean((meanheads - fielddata[yy, :]) ** 2))
        plt.figure(2)
        plt.plot(yy, rmse, 'xr')
        print('Residuals in time step %s is: %s' % (outputtimes[yy], rmse))
    plt.grid(True)
    plt.show()
    plt.close()

    ##    fig = plt.figure(figsize = (10,5), dpi = 100)
    ##    plt.plot(np.mean(ModelHeads, axis = 1), 'b.')



    ##    fig2 = plt.figure(figsize = (10,5), dpi = 100)
    ##    for ii in range(0,fielddata.shape[1]):
    ##        plt.plot(outputtimes, fielddata[:,ii],linestyle = '',marker = marker.next(), markersize = 7. )
    fig2 = plt.figure(figsize=(10, 5), dpi=100)
    for ii in range(0, fielddata.shape[1]):
        color = next(colors)
        plt.plot(outputtimes, fielddata[:, ii], linestyle='', marker='s', markersize=5., lw=0.01,
                 c=color)  # next(marker)

    for yy in range(0, len(lstresdir)):
        ModelHeads = np.load(os.path.join(resdir, lstresdir[yy]))
        meanheads = np.mean(ModelHeads, axis=1)
        # temp2 = np.reshape(meanheads, (-1,35), order = 'F')

        plt.plot(np.ones((len(meanheads), 1)) * outputtimes[yy], meanheads, linestyle='', marker='o', c='yellow',
                 ms=3.5, lw=0.01)
    plt.xlim([0, 1000])
    plt.grid(True)
    plt.show()
    plt.close()
    pdb.set_trace()

    # Plot histogram for the K-fields
    import matplotlib as mpl
    label_size = 20
    mpl.rcParams['xtick.labelsize'] = label_size
    mpl.rcParams['ytick.labelsize'] = label_size
    title_font = {'fontname': 'Arial', 'size': '22'}
    axis_font = {'fontname': 'Arial', 'size': '20'}
    n, bins, patches = plt.hist(np.log(orig_field), 30, normed=1, facecolor='green', alpha=0.75)
    import matplotlib.mlab as mlab
    y = mlab.normpdf(bins, -6, 0.63)
    l = plt.plot(bins, y, 'r--', linewidth=1, label='Gaussian pdf')
    plt.title('Probability distribution of log(K): $\mu=-6, \sigma=0.6$', **title_font)
    plt.xlabel('logK', **axis_font)
    plt.ylabel('Probability', **axis_font)
    plt.legend()
    plt.grid(True)
    plt.savefig('xxxx.png', transparent=True)
    plt.ticklabel_format(style='sci', axis='y', scilimits=(0, 0))
    plt.show()


if __name__ == '__main__':
    main()
